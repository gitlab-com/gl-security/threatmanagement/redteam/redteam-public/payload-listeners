## Nginx TLS Listener

### Current State: Non-functional proof of concept

### Usage

1. Generate your self-signed SSL certs with `bash ./gen-self-signed-certs.sh`
1. Adjust the port mappings in the docker-compose.yml file if needed
1. Use `docker-compose up -d --build` to launch the container
1. Use `curl -k https://localhost:<port>` to test your connection
1. Use `docker-compose down` to tear down the container
1. Logs are located in ./nginx/logs/access.log or error.log