#! /bin/bash

openssl req -newkey rsa:2048 -nodes -keyout ./nginx/localhost.key -x509 -days 365 -out nginx/localhost.crt