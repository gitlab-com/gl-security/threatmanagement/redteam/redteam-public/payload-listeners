## Python TLS Listener

### Current State: 1st Iteration

### Usage
1. Generate your self-signed SSL certs with `bash ./gen-self-signed-certs.sh`
1. Adjust the port mappings in the `pyserver.py` script file if needed
1. Run `bash ./start-server.sh` to start the server
1. Tail the logs with `bash ./tail-log.sh` if you're actively testing/watching for activity