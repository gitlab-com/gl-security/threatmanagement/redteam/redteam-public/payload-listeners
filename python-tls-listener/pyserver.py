from http.server import BaseHTTPRequestHandler, HTTPServer
from time import ctime
import ssl

class MyRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        # Send a 200 OK on POST requests
        self.send_response(200) 
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        # Read content-length/POST body 
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length).decode('utf-8')
        # Log POST body in log.txt file
        with open('log.txt', 'a') as log_file:
            log_file.write('[{}] {}\n'.format(ctime(), body))

# SSL context
context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
context.load_cert_chain('server.crt', 'server.key')

# Launching the httpd server
httpd = HTTPServer(('', 18007), MyRequestHandler)
httpd.socket = context.wrap_socket(httpd.socket, server_side=True)
httpd.serve_forever()
